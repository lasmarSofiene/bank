package com.kata.bank.operation;

public enum Type {

    WITHDRAW,
    DEPOSIT
}
