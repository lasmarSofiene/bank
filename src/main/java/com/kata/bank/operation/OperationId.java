package com.kata.bank.operation;


import com.kata.bank.account.Account;
import com.kata.bank.customer.Customer;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class OperationId implements Serializable {

    private static final long serialVersionUID = 3912193101593832821L;

    private Integer customerId ;
    private Integer accountId ;

    public OperationId(){}
    public OperationId(Integer customerId , Integer accountId){
        this.customerId = customerId ;
        this.accountId = accountId ;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
