package com.kata.bank.operation;

import com.kata.bank.account.Account;
import com.kata.bank.customer.Customer;
import com.kata.bank.exception.NegativeAmountException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank/operation/api")
@Api(value = "Operation Rest Controller")
public class OperationRestController  {

    @Autowired
    private OperationService operationService ;

    /**
     * add new operation with dataBase.
     *Update amountBalance with account
     */

    @PostMapping("/saveOperation")
    @ApiOperation(value = "Add a new Operation ", response = OperationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created: the Operation is successfully inserted"),
            @ApiResponse(code = 304, message = "Not Modified: the Operation is unsuccessfully inserted") })
    public ResponseEntity<Boolean> saveOperation(@RequestBody OperationDTO operationDTO) throws NegativeAmountException {

        Operation operationRequest = mapOperationDTOToOperation(operationDTO);

        operationService.operationTransaction(operationRequest.getAmount(), operationRequest.getType(),operationRequest.getOperationId());

        Operation operation =  operationService.saveOperation(operationRequest);
        if(operation != null){
            return  new ResponseEntity<Boolean>(true , HttpStatus.CREATED);
        }
        return new ResponseEntity<Boolean>(Boolean.FALSE , HttpStatus.NOT_MODIFIED);
    }


    private Operation mapOperationDTOToOperation(OperationDTO operationDTO){
        Operation operation = new Operation();
        Customer customer = new Customer();
        customer.setId(operationDTO.getCustomerDTO().getId());
        Account account = new Account();
        account.setId(operationDTO.getAccountDTO().getId());
        OperationId operationId = new OperationId(account.getId() , customer.getId());
        operation.setOperationId(operationId);
        operation.setAmount(operationDTO.getAmmount());
        if(operationDTO.getType().equals("WITHDRAW")){
            operation.setType(Type.WITHDRAW);
        }else if (operationDTO.getType().equals("DEPOSIT")){
            operation.setType(Type.DEPOSIT);
        }
        operation.setDateOperation(operationDTO.getDateOperation());
        return operation ;
    }
}
