package com.kata.bank.operation;

import com.kata.bank.account.Account;
import com.kata.bank.account.AccountRepository;
import com.kata.bank.exception.NegativeAmountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("operationService")
@Transactional
public class OperationServiceImpl implements OperationService {

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Operation saveOperation(Operation operation) {

        return operationRepository.save(operation);
    }

    @Override
    public List<Operation> findByAccount(Integer idaccount) {
        return null;
    }

    @Override
    public void operationTransaction(double amount ,Type type , OperationId operationId ) throws NegativeAmountException {
        if(amount < 0){
            throw new NegativeAmountException();
        }
        if(type == Type.WITHDRAW){

        }else if(type == Type.DEPOSIT){

        }
    }

    @Override
    public Operation findByOperationId(OperationId operationId) {
        return null;
    }

    private void makeOperation(double amount , Account account) {
      account.setAccountbalance(account.getAccountbalance() + amount );
      accountRepository.save(account);
    }
}
