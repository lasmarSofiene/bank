package com.kata.bank.operation;

import com.kata.bank.typeAccount.TypeAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long>, CrudRepository<Operation , Long> {

    @Override
    Optional<Operation> findById(Long aLong);

    Optional<Operation> findByOperationId(OperationId operationId);
}
