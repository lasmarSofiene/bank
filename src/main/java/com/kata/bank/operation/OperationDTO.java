package com.kata.bank.operation;

import com.kata.bank.account.AccountDTO;
import com.kata.bank.customer.CustomerDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ApiModel(value = "Opeartion Model")
public class OperationDTO {

    @ApiModelProperty(value = "Account concerned by the Operation")
    private AccountDTO accountDTO = new AccountDTO();

    @ApiModelProperty(value = "Customer concerned by The operation")
    private CustomerDTO customerDTO = new CustomerDTO();

    @ApiModelProperty(value = "Ammount ")
    private Double ammount ;

    @ApiModelProperty(value = "Date Operation")
    private LocalDate dateOperation ;

    @ApiModelProperty(value = "Type Operation")
    private String type ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public Double getAmmount() {
        return ammount;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public LocalDate getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(LocalDate dateOperation) {
        this.dateOperation = dateOperation;
    }
}
