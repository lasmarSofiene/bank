package com.kata.bank.operation;

import com.kata.bank.account.Account;
import com.kata.bank.exception.NegativeAmountException;

import java.util.List;
import java.util.Optional;

public interface OperationService {

    Operation saveOperation(Operation operation);

    List<Operation> findByAccount(Integer idaccount);

    void operationTransaction(double amount, Type type, OperationId operationId) throws NegativeAmountException;

    Operation findByOperationId(OperationId operationId);
}