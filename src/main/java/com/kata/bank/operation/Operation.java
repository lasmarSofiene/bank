package com.kata.bank.operation;

import com.kata.bank.account.Account;
import com.kata.bank.customer.Customer;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table (name = "OPERATION")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Operation {

    @EmbeddedId
    private OperationId operationId ;

   @Column(name = "DATE_OPERATION")
   private LocalDate dateOperation ;

    @Column(name = "AMOUNT")
    private Double amount ;

   @Enumerated(EnumType.STRING)
   @Column(name = "TYPE" , nullable = false)
   private Type type ;

    @ManyToOne
    private Account account ;

    @ManyToOne
    private Customer customer ;

    public Operation(Object o, LocalDateTime now, double v, Object o1) {
    }

    public OperationId getOperationId() {
        return operationId;
    }

    public void setOperationId(OperationId operationId) {
        this.operationId = operationId;
    }
}
