package com.kata.bank.customer;

import com.kata.bank.account.Account;
import com.kata.bank.operation.Operation;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="CUSTOMER")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Customer {

    @Id
    @Column(name = "CUSTOMER_ID")
    private Integer id ;

    @Column(name = "FIRST_NAME" , nullable = false)
    private String firstName ;

    @Column(name = "LAST_NAME" , nullable = false)
    private String lastName ;

    @Column(name = "ADRESS" , nullable = false)
    private String adress ;

    @Column(name = "EMAIL" , nullable = false)
    private String email ;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
    private Set<Operation> operation ;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
    private Set<Account> accountList ;

    public Customer(Integer id , String firstName , String lastName , String adress , String email){
        this.id =id ; this.firstName = firstName ; this.lastName = lastName ; this.adress = adress ; this.email = email ;
    }

}
