package com.kata.bank.account;

import java.util.List;
import java.util.Optional;

public interface AccountService {

      Account saveAccount(Account account);
      Account updateAccount(Account account );
      Optional<Account> findById(Integer id);
      void deleteAccount(Integer id);
      List<Account> getListAccountByTypeAccount(String codetypeaccount );
      List<Account> searchAccountByCustumer(Integer custumerId);


}
