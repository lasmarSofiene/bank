package com.kata.bank.account;

import com.kata.bank.customer.Customer;
import com.kata.bank.operation.Operation;
import com.kata.bank.typeAccount.TypeAccount;
import lombok.*;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ACCOUNT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ACCOUNT_ID")
    private Integer id ;

    @Column(name = "ACCOUNT_BALANCE")
    private double accountbalance ;

    @ManyToOne
    @JoinColumn(name="CUSTUMER_ID", nullable=false)
    private Customer customer ;

    @ManyToOne
    @JoinColumn(name="CODE_ACCOUNT" , referencedColumnName = "CODE_ACCOUNT")
    private TypeAccount typeAccount ;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private Set<Operation> operations ;

    public Account(Integer id ,Customer customer ,TypeAccount typeAccount , double accountbalance ){
        this.id = id ;
        this.customer = customer ;
        this.typeAccount = typeAccount ;
        this.accountbalance = accountbalance  ;
    }
}
