package com.kata.bank.account;

import com.kata.bank.customer.Customer;
import jdk.net.SocketFlow;
import org.modelmapper.ModelMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bank/account/api")
@Api(value = "Account Rest Controller: contains all operations for managment account")
public class AccountRestController {

    public static  final Logger LOGGER = LoggerFactory.getLogger(AccountRestController.class);

    @Autowired
    private AccountServiceImpl accountService ;

    @PostMapping("/addCompte")
    @ApiResponses(value = { @ApiResponse(code = 409, message = "Conflict: the account already exist"),
            @ApiResponse(code = 201, message = "Created: the account is successfully inserted"),
            @ApiResponse(code = 304, message = "Not Modified: the account is unsuccessfully inserted") })
    public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO accountDTORequest){
          Optional<Account> checkExisting = accountService.findById(accountDTORequest.getId());
          if(checkExisting.isPresent()){
              return new ResponseEntity<AccountDTO>(HttpStatus.CONFLICT);
          }
          Account accountRequest = mapAccountDTOToAccount(accountDTORequest);
          Account account = accountService.saveAccount(accountRequest);
          if(account !=null && account.getId()!=null){
              AccountDTO accountDTO = mapAccountToAccountDTO(account);
              return new ResponseEntity<AccountDTO>(accountDTO , HttpStatus.CREATED);
          }
          return  new ResponseEntity<AccountDTO>(HttpStatus.NOT_MODIFIED);
    }

    @PutMapping("/updateAccount")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok: the account is successfully updated"),
            @ApiResponse(code = 304, message = "Not Modified: the account is unsuccessfully updated") })
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody AccountDTO accountDTORequest){

        Account account = mapAccountDTOToAccount(accountDTORequest);
        Account acount = accountService.saveAccount(account);

        if(account !=null){

            AccountDTO accountDTO = mapAccountToAccountDTO(account);

            return new ResponseEntity<AccountDTO>(accountDTO , HttpStatus.OK);
        }
        return  new ResponseEntity<AccountDTO>(HttpStatus.NOT_MODIFIED);

    }

    @GetMapping("/listAccoutByCostumer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok: successfull "),
            @ApiResponse(code = 204, message = "No Content: no result founded"),
    })
    public ResponseEntity<List<AccountDTO>> searchAccountByCustomer(@RequestParam("custumerId") Integer custumerid){
       List<Account> accounts =  accountService.searchAccountByCustumer(custumerid);
        if(!accounts.isEmpty()){
       List<AccountDTO> accountDTOS =   accounts.stream().map(account -> {
            return  mapAccountToAccountDTO(account);
       }).collect(Collectors.toList());
        return new ResponseEntity<List<AccountDTO>>(accountDTOS , HttpStatus.OK);
        }
        return  new ResponseEntity<List<AccountDTO>>(HttpStatus.NO_CONTENT);
    }

    private Account mapAccountDTOToAccount(AccountDTO accountDTO) {
        ModelMapper mapper = new ModelMapper();
        Account account = mapper.map(accountDTO, Account.class);
        account.setCustomer(new Customer(accountDTO.getCustomer().getId() , accountDTO.getCustomer().getFirstName(),
        accountDTO.getCustomer().getLastName() , accountDTO.getCustomer().getAdress() , accountDTO.getCustomer().getEmail()));
        return account;
    }

    private AccountDTO mapAccountToAccountDTO(Account account) {
        ModelMapper mapper = new ModelMapper();
        AccountDTO accountDTO = mapper.map(account, AccountDTO.class);
        accountDTO.setCustomer(new Customer(account.getCustomer().getId() , account.getCustomer().getFirstName(),
                account.getCustomer().getLastName() , account.getCustomer().getAdress() , account.getCustomer().getEmail()));
        return accountDTO;
    }

}
