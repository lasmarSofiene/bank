package com.kata.bank.account;

import com.kata.bank.customer.Customer;
import com.kata.bank.typeAccount.TypeAccountDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Account Model")
public class AccountDTO {

    @ApiModelProperty(value = "Account id")
    private Integer id;

    @ApiModelProperty(value = "Account Balance")
    private double accountbalance ;

    @ApiModelProperty (value = "Acount Custumer")
    private Customer customer ;

    @ApiModelProperty(value = "Account Type")
    private TypeAccountDTO typeAccount ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getAccountbalance() {
        return accountbalance;
    }

    public void setAccountbalance(double accountbalance) {
        this.accountbalance = accountbalance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public TypeAccountDTO getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(TypeAccountDTO typeAccount) {
        this.typeAccount = typeAccount;
    }
}
