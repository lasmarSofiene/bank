package com.kata.bank.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Integer>, CrudRepository<Account , Integer> {


    Optional<Account> findById(Integer id);

    @Query("SELECT acc From Account acc inner join  acc.typeAccount typacc where typacc.code = :code ")
    List<Account> findByTypeAccount(@Param("code") String code);

    @Query("SELECT acc From Account acc inner join  acc.customer cust where cust.id = :id")
    List<Account> findByCustumer(@Param("id") Integer id) ;
}
