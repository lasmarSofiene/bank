package com.kata.bank.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service("accountService")
@Transactional
public class AccountServiceImpl implements  AccountService {

   @Autowired
   private AccountRepository accountRepository;

    @Override
    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account updateAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Optional<Account> findById(Integer id) {
       return accountRepository.findById(id);
    }

    @Override
    public void deleteAccount(Integer id) {

        accountRepository.deleteById(id);
    }

    @Override
    public List<Account> getListAccountByTypeAccount(String codetypeaccount) {
        return accountRepository.findByTypeAccount(codetypeaccount);
    }
    @Override
    public List<Account> searchAccountByCustumer(Integer custumerId) {
        return accountRepository.findByCustumer(custumerId);
    }
}
