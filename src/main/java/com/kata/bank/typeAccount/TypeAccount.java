package com.kata.bank.typeAccount;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Table
@Entity(name = "TYPE_COMPTE")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TypeAccount {
    @Id
    @Column(name = "CODE_ACCOUNT")
    private String code;
    @Column(name = "LABEL" , nullable = false)
    private String label;

}
