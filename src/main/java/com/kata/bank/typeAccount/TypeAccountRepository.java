package com.kata.bank.typeAccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TypeAccountRepository extends JpaRepository<TypeAccount , Long> , CrudRepository<TypeAccount , Long> {
}
