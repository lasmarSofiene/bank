package com.kata.bank.operation;

import com.kata.bank.account.Account;
import com.kata.bank.customer.Customer;
import com.kata.bank.exception.NegativeAmountException;
import com.kata.bank.typeAccount.TypeAccount;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class OperationTest {

    @Autowired
    OperationServiceImpl operationService ;


    @Test
    public void when_1000_are_deposited_then_account_balance_increases_by_1000() throws NegativeAmountException {

        Customer customer = new Customer(566 ,"alex" , "alex" ,"paris" , "alex.alex@gmail.com");
        TypeAccount typeaccount = new TypeAccount("code1" ,"current account");
        Account account = new Account(566 , customer ,typeaccount ,110  );
        double oldAccountBalance = account.getAccountbalance();
        OperationId operationId = new OperationId(account.getId() ,customer.getId());
        Type type = Type.DEPOSIT ;
        operationService.operationTransaction(1000 , type , operationId);

        assertThat(account.getAccountbalance() - oldAccountBalance).isEqualTo(1000);
    }

    @Test
    public void when_1000_are_withdrawed_then_account_balance_decreases_by_1000() throws NegativeAmountException {

        Customer customer = new Customer(566 ,"alex" , "alex" ,"paris" , "alex.alex@gmail.com");
        TypeAccount typeaccount = new TypeAccount("code1" ,"current account");
        Account account = new Account(566 , customer ,typeaccount ,110  );
        double oldAccountBalance = account.getAccountbalance();
        OperationId operationId = new OperationId(account.getId() ,customer.getId());
        Type type = Type.WITHDRAW ;
        operationService.operationTransaction(1000 , type , operationId);

        assertThat(account.getAccountbalance() - oldAccountBalance).isEqualTo(1000);


    }

    @Test(expected=IllegalArgumentException.class)
    public void when_amount_is_negative_then_should_throw_exception() {
        new Operation(null, LocalDateTime.now(),-1000.0 , null );
    }

}
