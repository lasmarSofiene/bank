package com.kata.bank.account;

import com.kata.bank.customer.Customer;
import com.kata.bank.typeAccount.TypeAccount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class AccountTest {

    @TestConfiguration
    static class AccountServiceImplTestContextConfiguration {
        @Bean
        public AccountService acountService() {
            return new AccountServiceImpl();
        }
    }
    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @Before
    public void setUp() {
       Customer customer = new Customer(566 ,"alex" , "alex" ,"paris" , "alex.alex@gmail.com");
        TypeAccount typeAccount = new TypeAccount("code1" ,"current account");
        Account account = new Account(566 , customer ,typeAccount ,110  );

       // Mockito.when(accountRepository.findByCustumer(customer.getId()))
           //     .thenReturn((List<Account>) account);
    }

    @Test
    public void whenValidIdCustumer_thenListAccountShouldBeFound(){
        Integer custumorId = 556 ;
        //List<Account> accounts = accountService.searchAccountByCustumer(custumorId);

       // assertThat(accounts.get(0).getId())
       // .isEqualTo(custumorId);
    }


}
